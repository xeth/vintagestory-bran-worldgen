# Bran's VintageStory WorldGen

Vintage Story world gen landforms modifications made by Bran.


# Build

This builds with `dotnet` used for VS modding.

1. Download C#/dotnet/VS code plugins: <https://code.visualstudio.com/docs/languages/dotnet>

2. Build mod .zip using:  
```
dotnet build -c release
```
This will generate an error that the `.dll` was not found because the
build script deletes the `.dll` before packing the `.zip`.
**Just ignore it.**

Alternatively, just zip up the files inside `resources/` to match 
the standard mod structure.